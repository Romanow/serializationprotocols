package ru.romanow.serialization.model;

/**
 * Created by ronin on 09.09.16
 */
public enum Status {
    DONE, FAIL, PAUSED
}
